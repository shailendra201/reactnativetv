import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View,Button,Alert } from 'react-native';
import { NativeRouter, Route, Link, Routes } from "react-router-native";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import Home from './Components/Home'
import Hair from './Components/Hair'
import Beauty from './Components/Beauty'


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' + 'Shake or press menu button for dev menu',
});

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home}  options={{ headerShown: false }}/>
        <Stack.Screen name="Hair" component={Hair}  options={{ headerShown: false }}/>
        <Stack.Screen name="Beauty" component={Beauty}  options={{ headerShown: false }}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;

// export default class App extends Component {


//   render() {

//     return (
//       <NativeRouter>
//     <View style={styles.container}>
//     <Routes>
//       <Route exact path="/" element={<Home />}  />
//       <Route path="/hair" element={<Hair />}  />
//       <Route path="/beauty" element={<Beauty />}  />
//     </Routes>
//     </View>
//   </NativeRouter>
//     );
//   }
// }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
