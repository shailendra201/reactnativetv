import React from 'react';
import { StyleSheet, Text, View, ImageBackground, Image } from 'react-native';
import { Link } from "react-router-native";
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class Beauty extends React.Component {
  state = {
    dealData:[]
  };
componentDidMount() {
   
    AsyncStorage.getItem('salonId', (err, result) => {
        if(result){
            this.getSalonDeals(result)
        }
    })

  }
  getMenuPrice(dealId){
    var x=0;
    this.state.dealData.forEach(function(deal){
       if(deal.dealId==dealId){
           x=deal.menuPrice
       }
    })
     return x
  }

  getMenuPrice2(dealId){
    var x=0;
    this.state.dealData.forEach(function(deal){
       if(deal.dealId==dealId){
           x=deal.menuPrice2
       }
    })
     return x
  }

  getDealPrice(dealId){

     var x=0;
     this.state.dealData.forEach(function(deal){
        if(deal.dealId==dealId){
            x=deal.price
        }
     })
      return x
  }

  getDealPrice2(dealId){
    
         var x=0;
         this.state.dealData.forEach(function(deal){
            if(deal.dealId==dealId){
                x=deal.price2
            }
         })
          return x
      }

  
  getSalonDeals(parlorId){
    var ins=this
    fetch('https://www.beusalons.com/apiv2/dealPriceByParlorAndDealId?parlorId='+parlorId)
    .then((response) => response.json())
    .then((responseJson) => {

     ins.setState({dealData:responseJson.data})
     
    })
    .catch((error) =>{
      console.error(error);
    });
  }

  render() {
    let data=[];
    return (
      <View style={styles.container}>
        <ImageBackground
          source={{ uri: "https://res.cloudinary.com/dyqcevdpm/image/upload/v1571136035/new-design/Background%20Images/tvmenubg.png" }}
          style={{ width: '100%', height: '100%' }}>
          <View style={{ flexDirection: 'row', paddingHorizontal: 30, paddingTop: 2 }}>
            <View style={{ flex: 46, justifyContent: 'center', alignItems: 'center'}}>
              <Text style={styles.titleTextBeauty}>BEAUTY DEAL MENU</Text>
            </View>
            <View style={{ flex:1, justifyContent: 'flex-end' }}>
              <Image
                source={{ uri: "https://res.cloudinary.com/dyqcevdpm/image/upload/v1571225379/TV_MENU/IMAGES/Master-Open-file-A5-Be-U-Smart-salon-logo.png" }}
                style={{ width: 110, height: 45, alignSelf: 'flex-end' }}
                resizeMode="contain"
              />
            </View>
          </View>
            <View style={{ flex: 6, flexDirection: 'row', paddingHorizontal: 45, paddingTop: 2 }}>
              <View style={{ flex: 1, marginRight: 20 }} >
                <View style={{ flex: 1, flexDirection: 'column' }}>
                  <View style={{ flex: 5, flexDirection: 'column' }}>
                    <Image
                      style={{height: 50, width: 300, marginVertical: 3, marginLeft: -10}}
                      resizeMode= 'contain'
                      source={{ uri: 'https://res.cloudinary.com/dyqcevdpm/image/upload/v1571212261/TV_MENU/IMAGES/Group_50.png' }}
                    />
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Full Arms</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(350):0}/{this.state.dealData[0]?this.getMenuPrice2(350):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(350):0}/{this.state.dealData[0]?this.getDealPrice2(350):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Full Legs</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(351):0}/{this.state.dealData[0]?this.getMenuPrice2(351):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(351):0}/{this.state.dealData[0]?this.getDealPrice2(351):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Underarms</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(352):0}/{this.state.dealData[0]?this.getMenuPrice2(352):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(352):0}/{this.state.dealData[0]?this.getDealPrice2(352):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Express Waxing</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(154):0}/{this.state.dealData[0]?this.getMenuPrice2(154):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(154):0}/{this.state.dealData[0]?this.getDealPrice2(154):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Full Body</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(17):0}/{this.state.dealData[0]?this.getMenuPrice2(17):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(17):0}/{this.state.dealData[0]?this.getDealPrice2(17):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Bikini</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(355):0}/{this.state.dealData[0]?this.getMenuPrice2(355):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(355):0}/{this.state.dealData[0]?this.getDealPrice2(355):0}</Text></View>
                    </View>
                  </View>
                  <View style={{ flex: 3, flexDirection: 'column' }}>
                    <Image
                      style={{height: 40, width: 120, marginVertical: 3, marginLeft: -10}}
                      resizeMode= 'contain'
                      source={{ uri: 'https://res.cloudinary.com/dyqcevdpm/image/upload/v1571212261/TV_MENU/IMAGES/Group_51.png' }}
                    />
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Luxury Facial</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(315):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(315):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Premium Facial</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(316):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(316):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Clean Up</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(317):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(317):0}</Text></View>
                    </View>
                  </View>
                  <View style={{ flex: 2, flexDirection: 'column' }}>
                    <Image
                      style={{height: 40, width: 120, marginVertical: 3, marginLeft: -10}}
                      resizeMode= 'contain'
                      source={{ uri: 'https://res.cloudinary.com/dyqcevdpm/image/upload/v1571212261/TV_MENU/IMAGES/Group_52.png' }}
                    />
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Face & Neck</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(155):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(155):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Eyebrow & Upperlips</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(156):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(156):0}</Text></View>
                    </View>
                  </View>
                </View>
              </View>
              <View style={{ flex: 1, marginLeft: 10 }} >
                <View style={{ flex: 1, flexDirection: 'column' }}>
                  <View style={{ flex: 8, flexDirection: 'column' }}>
                    <Image
                      style={{height: 50, width: 180, marginVertical: 3, marginLeft: -10}}
                      resizeMode= 'contain'
                      source={{ uri: 'https://res.cloudinary.com/dyqcevdpm/image/upload/v1571212261/TV_MENU/IMAGES/Group_49.png' }}
                    />
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Eye Brow</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(361):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(361):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Upperlips</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(362):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(362):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Eyebrow & Upperlips</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(156):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(156):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Forehead/Chin</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(363):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(363):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Sides & Jawline</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(364):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(364):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Full Face</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(365):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(365):0}</Text></View>
                    </View>
                  </View>
                  <View style={{ flex: 7, flexDirection: 'column' }}>
                    <Image
                      style={{height: 50, width: 300, marginVertical: 3, marginLeft: -10}}
                      resizeMode= 'contain'
                      source={{ uri: 'https://res.cloudinary.com/dyqcevdpm/image/upload/v1571212261/TV_MENU/IMAGES/Group_53.png' }}
                    />
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Classic</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(366):0}/{this.state.dealData[0]?this.getMenuPrice(369):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(366):0}/{this.state.dealData[0]?this.getDealPrice(369):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Premium</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(367):0}/{this.state.dealData[0]?this.getMenuPrice(326):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(367):0}/{this.state.dealData[0]?this.getDealPrice(326):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Luxury</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(368):0}/{this.state.dealData[0]?this.getMenuPrice(327):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(368):0}/{this.state.dealData[0]?this.getDealPrice(327):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Classic Mani + Padi</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(373):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(373):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Premium Mani + Padi</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(324):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(324):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Luxury Mani + Padi</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(325):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(325):0}</Text></View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center', paddingHorizontal: 45, marginTop: 10}}>
              <Text style={styles.titleTextBeauty}>MAKE UP DEAL MENU</Text>
              </View>
            <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 45 }}>
              <View style={{ flex: 1, marginRight: 20 }}>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                  <View style={{ flex: 2, flexDirection: 'column' }}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Saree Draping</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(159):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(159):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Light Makeup</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(24):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(24):0}</Text></View>
                    </View>
                  </View>
                </View>
              </View>
              <View style={{ flex: 1, marginLeft: 10 }} >
                <View style={{ flex: 1, flexDirection: 'column' }}>
                  <View style={{ flex: 2, flexDirection: 'column' }}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Occasion Makeup</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(25):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(25):0}</Text></View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 3 }}><Text style={styles.headingfontBeauty}>Bridal Makeup</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.cutpriceBeauty}>{this.state.dealData[0]?this.getMenuPrice(26):0}</Text></View>
                      <View style={{ flex: 2, alignItems: 'center' }}><Text style={styles.normalpriceBeauty}>{this.state.dealData[0]?this.getDealPrice(26):0}</Text></View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  titleTextBeauty: {
    fontSize: 36,
    fontWeight: 'bold',
    color: 'green',
  },
  cutpriceBeauty: {
    textDecorationLine: 'line-through',
    textAlign: 'right',
    alignSelf: 'stretch',
    fontSize: 19,
    color: '#808080',
  },
  normalpriceBeauty: {
    textAlign: 'right',
    alignSelf: 'stretch',
    fontSize: 23,
    fontWeight: '600'
  },
  headingfontBeauty: {
    fontSize: 22,
  }
});