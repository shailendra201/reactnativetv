import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {Picker} from '@react-native-picker/picker';
import { TextInput, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TVEventHandler } from 'react-native';


export default class Home extends React.Component {

  tvEventHandler;

  constructor(props) {
    super(props);
    console.log("props");
    console.log(this.props.navigation);
    this.state = { modalVisible: false, dealData: [], TVId: "", salonId: "" };
    this.inputRef = React.createRef();
    this.onChangeSalonId = this.onChangeSalonId.bind(this);
    // this.enableTVEventHandler = this.enableTVEventHandler.bind(this);
  }
  enableTVEventHandler() {
    this.tvEventHandler = new TVEventHandler();
    // console.log('tvEventHandler', this.tvEventHandler.enable);
    this.tvEventHandler.enable(this, function(cmp, evt) {
      if (evt) {
        
      }
    });
  }
  componentDidMount() {
    if(this.inputRef.focus){
      this.inputRef.focus();
    }
    // this.enableTVEventHandler();
    console.log("inside component did mount");
    AsyncStorage.getItem('salonId', (err, result) => {
      console.log("reuslt of storage api", err, result);
      if (!result) {
        this.setState({ modalVisible: true })

      }else{
        console.log("result from local storage is ",result);
        this.getSalonDeals(result)
      }
    });
  }


   
  handleClick() {
      history.push("/home");
    }
  
  getSalonDeals(parlorId){
    
    var ins=this
    fetch('https://www.beusalons.com/apiv2/dealPriceByParlorAndDealId?parlorId='+parlorId)
    .then((response) => response.json())
    .then((responseJson) => {
     ins.setState({dealData:responseJson.data})
     AsyncStorage.getItem('TVId', (err, result) => {
        if(result=="1"){
          this.props.navigation.navigate('Hair')
         }else if(result=="2"){
          this.props.navigation.navigate('Beauty')
         }else{
          this.props.navigation.navigate('Hair')
         }


       
     })
    })
    .catch((error) =>{
      console.error(error);
    });
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  getData() {
    console.log("get data called")
    if (this.state.salonId && this.state.TVId) {
      AsyncStorage.setItem('salonId',this.state.salonId , () => {
        AsyncStorage.setItem('TVId',this.state.TVId , () => {
          
          this.setState({ modalVisible: false });
          this.getSalonDeals(this.state.salonId);

        });
      });
      

    } else {
      console.log("value puri nhi aayi")
      Alert.alert('Enter all values.');
    }
  }
  onChangeTVId(text) {
    if(text!=" "){
      this.setState({ TVId: text },function(){
        Alert.alert('Fetching Details');
        this.getData()
      })
    }
  }
  onChangeSalonId(text) {
    this.setState({ salonId: text })
  }
  render() {
    // console.log("inside render");
    return (
      <View style={styles.container}>
        <Text> Add Details Here</Text>
        <View>
              <Text>Enter Salon Id</Text>
              <TextInput
                keyboardType='numeric'
                placeholder='salon id'
                onChangeText={this.onChangeSalonId}
                value={this.state.salonId}
                style={styles.input}
                ref={(ref) => { this.inputRef = ref }}
                // ref={this.inputRef}
                />
              {/* <Picker
              selectedValue={this.state.salonId}
              style={{height: 50, width: 100}}
              onValueChange={(itemValue, itemIndex) =>
                this.onChangeSalonId(itemValue)
              }>
              <Picker.Item label="1" value="1" />
              <Picker.Item label="2" value="2" />
              <Picker.Item label="3" value="3" />
              <Picker.Item label="4" value="4" />
              <Picker.Item label="5" value="5" />
              <Picker.Item label="6" value="6" />
              <Picker.Item label="7" value="7" />
              <Picker.Item label="8" value="8" />
              <Picker.Item label="9" value="9" />
              <Picker.Item label="10" value="10" />
              <Picker.Item label="11" value="11" />
              <Picker.Item label="12" value="12" />
              <Picker.Item label="13" value="13" />
              <Picker.Item label="14" value="14" />
              <Picker.Item label="15" value="15" />
              <Picker.Item label="16" value="16" />
              <Picker.Item label="17" value="17" />
              <Picker.Item label="18" value="18" />
              <Picker.Item label="19" value="19" />
              <Picker.Item label="20" value="20" />
              <Picker.Item label="21" value="21" />
              <Picker.Item label="22" value="22" />
              <Picker.Item label="23" value="23" />
              <Picker.Item label="24" value="24" />
              <Picker.Item label="25" value="25" />
              <Picker.Item label="26" value="26" />
              <Picker.Item label="27" value="27" />
              <Picker.Item label="28" value="28" />
              <Picker.Item label="29" value="29" />
              <Picker.Item label="30" value="30" />
              <Picker.Item label="31" value="31" />
              <Picker.Item label="32" value="32" />
              <Picker.Item label="33" value="33" />
              <Picker.Item label="34" value="34" />
              <Picker.Item label="35" value="35" />
              <Picker.Item label="36" value="36" />
              <Picker.Item label="37" value="37" />
              <Picker.Item label="38" value="38" />
              <Picker.Item label="39" value="39" />
              <Picker.Item label="40" value="40" />
              <Picker.Item label="41" value="41" />
              <Picker.Item label="42" value="42" />
              <Picker.Item label="43" value="43" />
              <Picker.Item label="44" value="44" />
              <Picker.Item label="45" value="45" />
              <Picker.Item label="46" value="46" />
              <Picker.Item label="47" value="47" />
              <Picker.Item label="48" value="48" />
              <Picker.Item label="49" value="49" />
              <Picker.Item label="50" value="50" />
              <Picker.Item label="51" value="51" />
              <Picker.Item label="52" value="52" />
              <Picker.Item label="53" value="53" />
              <Picker.Item label="54" value="54" />
              <Picker.Item label="55" value="55" />
              <Picker.Item label="56" value="56" />
              <Picker.Item label="57" value="57" />
              <Picker.Item label="58" value="58" />
              <Picker.Item label="59" value="59" />
              <Picker.Item label="60" value="60" />
              <Picker.Item label="60" value="60" />
              <Picker.Item label="61" value="61" />
              <Picker.Item label="62" value="62" />
              <Picker.Item label="63" value="63" />
              <Picker.Item label="64" value="64" />
              <Picker.Item label="65" value="65" />
              <Picker.Item label="66" value="66" />
              <Picker.Item label="67" value="67" />
              <Picker.Item label="68" value="68" />
              <Picker.Item label="69" value="69" />
              <Picker.Item label="70" value="70" />

            </Picker> */}
              <Text style={{ paddingTop: 10 }}>Enter TV Id</Text>
              <Picker
              selectedValue={this.state.TVId}
              style={{height: 50, width: 100}}
              selectedValue={" "}
              onValueChange={(itemValue, itemIndex) =>
                this.onChangeTVId(itemValue)
              }>
               <Picker.Item label=" " value=" " />
              <Picker.Item label="1" value="1" />
              <Picker.Item label="2" value="2" />
            </Picker>
            </View>
        {/* <Link to="/hair" underlayColor="#f0f4f7" style={styles.navItem}>
          <Text>Hair</Text>
        </Link>
        <Link to="/beauty" underlayColor="#f0f4f7" style={styles.navItem}>
          <Text>Beauty</Text>
        </Link> */}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 100,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  navItem: {
    alignItems: "center",
    padding: 10
  },
  input: {
    paddingHorizontal: 5,
    marginVertical: 5,
    borderWidth: 1,
    borderRadius: 5
  }
});

// modal: {
//   justifyContent: 'center',  
//   alignItems: 'center',   
//   backgroundColor : "#00BCD4",   
//   height: 300 ,  
//   width: '80%',  
//   borderRadius:10,  
//   borderWidth: 1,  
//   borderColor: '#fff',
//   padding: 40,
//    },