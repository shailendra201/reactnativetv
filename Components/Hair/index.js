import React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground } from 'react-native';
import { Link } from "react-router-native";
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class Hair extends React.Component {
    state = {
        dealData:[]
      };
    componentDidMount() {
        AsyncStorage.getItem('salonId', (err, result) => {
            if(result){
                this.getSalonDeals(result)
            }
        })
    
      }
      getMenuPrice(dealId){
        var x=0;
        this.state.dealData.forEach(function(deal){
           if(deal.dealId==dealId){
               x=deal.menuPrice
           }
        })
         return x
      }
      getDealPrice(dealId){
         var x=0;
         this.state.dealData.forEach(function(deal){
            if(deal.dealId==dealId){
                x=deal.price
            }
         })
          return x
      }
      getSalonDeals(parlorId){
        var ins=this
        fetch('https://www.beusalons.com/apiv2/dealPriceByParlorAndDealId?parlorId='+parlorId)
        .then((response) => response.json())
        .then((responseJson) => {

         ins.setState({dealData:responseJson.data})
         
        })
        .catch((error) =>{
          console.error(error);
        });
      }
    render() {
        let data=[];

        return (
            <View style={styles.container}>
                <ImageBackground
                    source={{ uri: "https://res.cloudinary.com/dyqcevdpm/image/upload/v1571136035/new-design/Background%20Images/tvmenubg.png" }}
                    style={{ width: '100%', height: '100%' }}>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 30, paddingTop: 2}}>
                        <View style={{ flex: 46, justifyContent: 'center',alignItems: 'center'}}>
                            <Text style={styles.titleText}>HAIR DEAL MENU</Text>
                        </View>
                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                            <Image
                                source={{ uri: "https://res.cloudinary.com/dyqcevdpm/image/upload/v1571225379/TV_MENU/IMAGES/Master-Open-file-A5-Be-U-Smart-salon-logo.png" }}
                                style={{ width: 110, height: 45, alignSelf: 'flex-end' }}
                                resizeMode="contain"
                                resizeMethod="resize" />
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 45, paddingTop: 6 }}>
                        <View style={{ flex: 1, marginRight: 20 }} >
                            <View style={{ flex: 1, flexDirection: 'column' }}>
                                <View style={{ flex: 4, flexDirection: 'column' }}>
                                    <Image
                                    style={{height: 50, width: 250, marginVertical: 3, marginLeft: -10}}
                                    resizeMode= 'contain'
                                        source={{ uri: 'https://res.cloudinary.com/dyqcevdpm/image/upload/v1571212260/TV_MENU/IMAGES/Group_43.png' }}
                                    />
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Smoothening/ Straightening/ Rebonding</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(1):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(1):0}</Text></View>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Keratin</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(2):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(2):0}</Text></View>
                                    </View>
                                </View>
                                <View style={{ flex: 5, flexDirection: 'column' }}>
                                    <Image
                                    style={{height: 50, width: 250, marginVertical: 5, marginLeft: -10}}
                                    resizeMode= 'contain'
                                        source={{ uri: 'https://res.cloudinary.com/dyqcevdpm/image/upload/v1571212260/TV_MENU/IMAGES/Group_44.png' }}
                                    />
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Global Color</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(3):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(3):0}</Text></View>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Highlights</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(4):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(4):0}</Text></View>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Creative Color</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(5):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(5):0}</Text></View>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Color Change</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(6):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(6):0}</Text></View>
                                    </View>
                                </View>
                                <View style={{ flex: 3, flexDirection: 'column' }}>
                                    <Image
                                    style={{height: 50, width: 220, marginVertical: 8, marginLeft: -10}}
                                    resizeMode= 'contain'
                                        source={{ uri: 'https://res.cloudinary.com/dyqcevdpm/image/upload/v1571212260/TV_MENU/IMAGES/Group_45.png' }}
                                    />
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Root Touch Up</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(93):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(93):0}</Text></View>
                                    </View>
                                </View>
                                <View style={{ flex: 4, flexDirection: 'column' }}>
                                    <Image
                                    style={{height: 50, width: 220, marginVertical: 3, marginLeft: -10}}
                                    resizeMode= 'contain'
                                    source={{ uri: 'https://res.cloudinary.com/dyqcevdpm/image/upload/v1571212260/TV_MENU/IMAGES/Group_46.png' }}
                                    />
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Loreal Hair Spa</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(322):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(322):0}</Text></View>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Expert Treatment</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(323):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(323):0}</Text></View>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Ampule</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(39):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(39):0}</Text></View>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={{ flex: 1, marginLeft: 10 }} >
                            <View style={{ flex: 1, flexDirection: 'column' }}>
                                <View style={{ flex: 6, flexDirection: 'column' }}>
                                    <Image
                                    style={{height: 50, width: 160, marginVertical: 3, marginLeft: -10}}
                                    resizeMode= 'contain'
                                        source={{ uri: 'https://res.cloudinary.com/dyqcevdpm/image/upload/v1571212260/TV_MENU/IMAGES/Group_47.png' }}
                                    />
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Global Color + Highlights</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(30):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(30):0}</Text></View>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Global Color + Keratin</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(28):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(28):0}</Text></View>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Highlights + Keratin</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(31):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(31):0}</Text></View>
                                    </View>
                                    {/* <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Smoothening + Keratin</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(39):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(39):0}</Text></View>
                                    </View> */}
                                </View>
                                <View style={{ flex: 10, flexDirection: 'column' }}>
                                    <Image
                                    style={{height: 50, width: 160, marginVertical: 3, marginLeft: -10}}
                                    resizeMode= 'contain'
                                        source={{ uri: 'https://res.cloudinary.com/dyqcevdpm/image/upload/v1571212260/TV_MENU/IMAGES/Group_48.png' }}
                                    />
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Female Cut</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(36):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(36):0}</Text></View>
                                    </View>
                                    {
                                        this.getMenuPrice(37) ? 
                                        (<View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Male Cut</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(37):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(37):0}</Text></View>
                                    </View>) : null

                                    }
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Blowdry</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(38):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(38):0}</Text></View>
                                    </View>
                                    {
                                        this.getMenuPrice(73) ? 
                                        ( <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Beard Trimminng</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(73):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(73):0}</Text></View>
                                    </View>) : null
                                    }
                                   {
                                    this.getMenuPrice(69) ? 
                                    (<View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Shaving</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(69):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(69):0}</Text></View>
                                    </View>): null
                                   }
                                    {
                                    this.getMenuPrice(299) ?
                                    (<View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Styling Shave</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(299):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(299):0}</Text></View>
                                    </View>): null
                                    }
                                   
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Children Cut (Under 10)</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(67):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(67):0}</Text></View>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Shampoo & Conditioner (Female)</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(348):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(348):0}</Text></View>
                                    </View>
                                    {
                                        this.getMenuPrice(372) ? 
                                        (<View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 4 }}><Text style={styles.headingfont}>Shampoo & Conditioner (Male)</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.cutprice}>{this.state.dealData[0]?this.getMenuPrice(372):0}</Text></View>
                                        <View style={{ flex: 1, alignItems: 'center' }}><Text style={styles.normalprice}>{this.state.dealData[0]?this.getDealPrice(372):0}</Text></View>
                                    </View>) : null
                                    }
                                    
                                </View>
                            </View>
                        </View>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    titleText: {
        fontSize: 36,
        fontWeight: 'bold',
        color: 'green',
    },
    cutprice: {
        textDecorationLine: 'line-through',
        textAlign: 'right',
        alignSelf: 'stretch',
        fontSize: 19,
        color: '#808080',
    },
    normalprice: {
        textAlign: 'right',
        alignSelf: 'stretch',
        fontSize: 23,
        fontWeight: '600'
    },
    headingfont: {
        fontSize: 22,
    }
});